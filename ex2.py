import os
import requests
import shutil
import yagmail

results = requests.get('https://jsonplaceholder.typicode.com/photos')
urls = []

for index in results.json():
    urls.append(index['url'])

os.mkdir('pasta_imagens')

for index in range(500):
    imagem = open(f'./pasta_imagens/imagem {index+1}.jpg', 'wb')
    response = requests.get(urls[index])
    imagem.write(response.content)
    imagem.close()

shutil.make_archive('imagens', 'zip', './', './pasta_imagens')

password = input("senha: ")
yag = yagmail.SMTP(user='vinny.said@gmail.com', password= password)
# sending the email
yag.send(to='osenias.oliveira@fpf.br', subject='DESAFIO 2 ', contents='Segue arquivo .zip de imagens baixadas', attachments="imagens.zip")
print("Email enviado.")

os.system(f'rm -rf ./pasta_imagens')
os.system(f"rm -rf ./imagens.zip")
print("Arquivos deletados")